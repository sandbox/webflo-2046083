<?php

/**
 * @file
 * Contains \Drupal\domain_locale\EventSubscriber\DomainLocaleSubscriber.php
 */

namespace Drupal\domain_locale\EventSubscriber;

use Drupal\Core\Config\ConfigEvent;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManager;
use Drupal\domain\DomainInterface;
use Drupal\domain\DomainManagerInterface;
use Drupal\Core\Config\Context\ContextInterface;
use Drupal\domain_config\EventSubscriber\DomainConfigSubscriber;
use Drupal\language\LanguageInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Implements DomainLocaleSubscriber
 */
class DomainLocaleSubscriber implements EventSubscriberInterface {

  /**
   * The domain manager.
   *
   * @var \Drupal\domain\DomainManagerInterface
   */
  protected $domainManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * The default configuration context.
   *
   * @var \Drupal\Core\Config\Context\ConfigContext
   */
  protected $defaultConfigContext;

  /**
   * Constructs a DomainLocaleSubscriber object.
   *
   * @param DomainManagerInterface $domain_manager
   * @param LanguageManager $language_manager
   * @param ContextInterface $config_context
   */
  public function __construct(DomainManagerInterface $domain_manager, LanguageManager $language_manager, ContextInterface $config_context) {
    $this->domainManager = $domain_manager;
    $this->languageManager = $language_manager;
    $this->defaultConfigContext = $config_context;
  }

  /**
   * Override configuration values with domain-localized data.
   */
  public function configLoad(ConfigEvent $event) {
    $context = $event->getContext();

    $domain = $context->get('domain_config.domain');
    $language = $context->get('locale.language');

    if (isset($domain) && isset($language)) {
      $config = $event->getConfig();
      $config_name = $this->getDomainLocaleConfigName($config->getName(), $domain, $language);
      if ($override = $event->getConfig()->getStorage()->read($config_name)) {
        $config->setOverride($override);
      }
    }
  }

  /**
    * Get configuration name for domain-language pair.
   *
   * Name: domain_locale.config.DOMAIN.LANGCODE.NAME
   */
  public function getDomainLocaleConfigName($name, $domain, $language) {
    return 'domain_locale.config.' . $domain->machine_name . '.' . $language->id . '.' . $name;
  }

  /**
   * Implements EventSubscriberInterface::getSubscribedEvents().
   */
  static function getSubscribedEvents() {
    // Lower priority than DomainConfigSubscriber and LocaleConfigSubscriber
    $events['config.load'][] = array('configLoad', 10);
    return $events;
  }

}

