# Domain and Locale Configuration

Allows domain and language specific configuration.

## Priority of configuration files

Specific to generic.

- domain_locale.config.eaxmple_com.de.system.site.yml
- domain_config.config.example_com.system.site.yml
- locale.config.de.system.site.yml
- system.site.yml

See docs/example_config for example configuration with Site Name.
